\documentclass[../main/main.tex]{subfiles}

\begin{document}
\section{Фундаментальные каталоги}
\noindent\textbf{Фундаментальный каталог}~--- каталог, задающий систему отсчета (данные абсолютным методом).\\
Рассматриваемые в~этом разделе алгоритмы и~методы применимы и~к~обычным каталогам.

\subsection{Астрометрические каталоги}
\noindent Любой наблюдательный каталог содержит следующую информацию: $N$, $\alpha$, $\delta$, $t_{\alpha}$, $t_{\delta}$.
Здесь $t_{\alpha}, \, t_{\delta}$~--- эпохи получения~$\alpha$ и~$\delta$,
координаты отнесены к~одному равноденствию, а~$N$~--- идентификатор звезды, например номер.\\
\textbf{Эпоха}~--- это средний момент наблюдений с~целью определения координат.\\
\textbf{Эпоха}~--- это момент, на~который с~учетом собственного движения переведены координаты.

\medskip
\noindent\textbf{Равноденствие каталога}~--- это момент, на~который зафиксированы нуль пункты~$\alpha$ и~$\delta$ каталога.
Например: $1900.0$, $1950.0$, $2000.0$. Переход с~одного равноденствия на~другое делается с~учетом \textit{только} прецессии.
Координаты могут быть абсолютными, могут быть относительными, в~любом случае они получаются по~схеме:
\vspace{0.2\baselineskip}
\par\noindent \fbox{инструмент} \; --- \; \fbox{приемник} \; --- \; \fbox{метод обработки} $\rightarrow X = x + \xi$,
\vspace{0.2\baselineskip}
\par\noindent $X$~--- либо $\alpha$, либо $\delta$; $x$~--- точное значение; $\xi$~--- ошибка, в~свою очередь $\xi = \xi_r + \xi_s$.

\medskip
\noindent\textbf{Случайные ошибки:}\\
Случайные ошибки не~зависят от~устойчивых параметров звезды (положение на~небесной сфере, яркость, спектр и~т.д.),
уровень ошибок характеризуется среднеквадратичной ошибкой. Пусть имеется ряд наблюдений \mbox{$x_1$, $x_2$,..., $x_n$}.
Среднее значение выбирается в~качестве наилучшего приближения.
Вычисляем:
$$
x_0 = \frac{1}{n} \sum_{k=1}^n x_k, \qquad \sigma_0^2 = \frac{1}{n-1} \sum_{k=1}^n (x_k - x_0)^2, \qquad \sigma_n^2 = \frac{\sigma_0^2}{n}.
$$
\begin{wrapfigure}{r}{4.5cm}
    \centering \includegraphics[scale=1.0]{../section04/images/ErrorGraph/ErrorGraph.pdf}
\end{wrapfigure}
$\sigma_0$~--- среднеквадратичная ошибка одного измерения, характеризующая точность инструмента.
Типовые примеры:
\begin{itemize}[noitemsep,topsep=0pt,leftmargin=*]
    \item Классическая астрометрия: $\sigma_0 = 0''.15 \div 0''.30$
    \item Космическая астрометрия: $\sigma_0 = 0''.01$ (для HIPPARCOS)
    \item РСДБ астрометрия: $\sigma_0 = 0''.001$
\end{itemize}
$\sigma_n$~--- среднеквадратичная ошибка совокупности измерений:
$$
\sigma_n^2 = \sigma_{\infty}^2 + \frac{\sigma_0^2}{n},
$$
где $\sigma_{\infty}$~--- уровень неучтенных систематических ошибок. Обычно $n \le 100$, поэтому $\sigma_n$ снижается на~порядок.
Источники случайных ошибок~--- непредсказуемые во~время наблюдения инструментальные эффекты.
Например, неучтенные случайные колебания среды.
Также может иметь место неподвижка инструмента ("кто-то облокотился"), технические проблемы аппаратуры.
Вещи, которые не~можем формализовать на~долгом этапе.
Для~РСДБ: вплоть до~влияния микроволновки на~данные сигналов. 

\medskip
\noindent\textbf{Систематические ошибки:}\\
Систематические ошибки зависят от~координат, блеска, спектра и~других устойчивых параметров.\\
\underline{Источники систематических ошибок:}
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*,label=\textbf{{\arabic*.}}]
    \item Погрешности инструмента.
    \item Влияние окружающей среды (температура, давление, влажность).
    \item Методы обработки наблюдений (могут быть довольно важны).
\end{enumerate}

\par\noindent В астрометрии приняты следующие классификации систематических ошибок:
\begin{itemize}[noitemsep,topsep=0pt,leftmargin=*]
    \item $\Delta \alpha = \Delta A + \Delta \alpha_{\delta} + \Delta \alpha_{\alpha} + \Delta \alpha_m + ...,$
    \item $\Delta \delta = \Delta D + \Delta \delta_{\delta} + \Delta \delta_{\alpha} + \Delta \delta_m + ...,$
    \item $\Delta \mu = \Delta \mu_{\delta} + \Delta \mu_{\alpha} + \mu_m,$
\end{itemize}

\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*,label=\arabic*)]
    \item $\Delta A, \Delta D$~--- ошибки нуль-пунктов, общие для всех звезд,
    \item $\Delta \delta_{\delta}, \Delta \alpha_{\delta}, \Delta \mu_{\delta}$~--- непериодические ошибки, одна из основных причин~--- гнутся инструменты,
    \item $\Delta \delta_{\alpha}, \Delta \alpha_{\alpha}, \Delta \mu_{\alpha}$~--- периодические ошибки, возникающие от воздействия окружающей среды,
    \item $\Delta \alpha_m, \Delta \delta_m, \Delta \mu_m$~--- ошибки уравнения яркости (которые индивидуальны для каждой звезды),
\end{enumerate}
и другие.

\newpage
\begin{figure}[ht]
\begin{center}
    \begin{center}
        \subfloat[Координатная сетка]{
        \includegraphics[width=0.28\textwidth]{../section04/images/SystematicError0/SystematicError0.pdf}}\hfill
        \subfloat[$\Delta \alpha_{\alpha}$]{
        \includegraphics[width=0.28\textwidth]{../section04/images/SystematicError1/SystematicError1.pdf}}\hfill
        \subfloat[$\Delta \delta_{\delta}$]{
        \includegraphics[width=0.28\textwidth]{../section04/images/SystematicError2/SystematicError2.pdf}}
    \end{center}
    \begin{center}
        \subfloat[$\Delta \delta_{\alpha}$]{
        \includegraphics[width=0.28\textwidth]{../section04/images/SystematicError3/SystematicError3.pdf}}\hfill
        \subfloat[$\Delta \alpha_{\delta}$]{
        \includegraphics[width=0.28\textwidth]{../section04/images/SystematicError4/SystematicError4.pdf}}\hfill
        \subfloat[$\Delta \alpha_m, \Delta \delta_m$]{
        \includegraphics[width=0.28\textwidth]{../section04/images/SystematicError5/SystematicError5.pdf}}
    \end{center}
    \subfloat[Общее действие]{
        \begin{minipage}{0.28\textwidth}
            \centering \includegraphics[width=\textwidth]{../section04/images/SystematicErrorG/SystematicErrorG.pdf}
        \end{minipage}
        \hspace*{0.5cm}
        \begin{minipage}[c]{0.4\textwidth}
            \hspace{\parindent} Общий уровень систематических ошибок:
            \begin{itemize}[noitemsep]
                \item Классическая астрометрия $\sim 0''.1$
                \item РСДБ $\sim 1$~mas
                \item Космическая астрометрия $< 1$~mas
            \end{itemize}
            \medskip
            
            Смещение $\Delta \delta_{\delta}$ зависит от~$\alpha$!
            В~общем случае не~параллельный перенос.
            $\Delta \alpha_{\alpha}$~аналогично.
        \end{minipage}}
    \end{center}
    \caption{Геометрический смысл систематических ошибок}
    \label{GeomSystematicErrs}
\end{figure}

\subsection{Систематические разности двух каталогов}
\begin{multicols}{2}
\noindent
Представим информацию в каталоге в виде:\\
$k = k_0 + s + r$,\\
$k_0$~--- точный результат,\\
$s$~--- систематическая ошибка,\\
$r$~--- случайная ошибка.

\medskip
\noindent\textbf{Система каталога} --- это точные решения, искаженные систематическими ошибками.\\
$S = k_0 + s$\\
Сравнение каталогов:\\
$k_1 = k_0 + s_1 + r_1 = S_1 + r_1$,\\
$k_2 = k_0 + s_2 + r_2 = S_2 + r_2$.\\
Индивидуальные разности:\\
$\Delta k_{1,2} = \Delta S + \Delta r$.

%\vfill\null\columnbreak

\medskip
\par\noindent\textbf{Системные разности $\Delta S$}~--- это~разности систем каталогов.
\par\noindent\textbf{Получение систематической разности}~--- это~освобождение индивидуальных разностей от~случайных компонент.

\medskip
\noindent $\Delta S = \mathcal{F} \{ \Delta k_{1,2} \}$, $\mathcal{F}$~--- низкочастотный фильтр.
Считается, что~систематические ошибки имеют низкие частоты, т.е.~долгопериодические.
А~случайные~--- высокочастотные, т.е.~короткопериодические.
Сглаживание можно рассматривать как~низкочастотный фильтр.
$$
\left.\begin{array}{l}
k_1 \rightarrow k_2 \qquad k_2 = k_1 - \Delta S \\
k_2 \rightarrow k_1 \qquad k_1 = k_2 + \Delta S \\
\end{array} \right\} \text{ редукции.}
$$
\end{multicols}

\subsubsection{Классический (табличный) метод получения систематических разностей}
\noindent Исходные данные $\Delta \alpha ,\ \cos \delta, \Delta \delta, \Delta \mu \, \cos \delta, \Delta \mu'$.
В~общем случае считаем, что~$f = f (\alpha, \delta)$.
Перед получением индивидуальных разностей каталоги приводят к~общему равноденствию и~общей эпохе.
Ниже представлена пошаговая схема получения систематических разностей:

\begin{enumerate}[leftmargin=*,label=\textbf{{\arabic*.}}]
    \item Осредняем индивидуальные разности по сферическим площадкам~--- разбиваем экватор на~N частей, небесный меридиан на~M~частей.
        Вводим центры сферических трапеций:
        $$
        \alpha_i = \dfrac{24^h}{N} \left( i + \dfrac{1}{2} \right), \quad i = \overline{0, N-1}, \qquad
        \delta_k = 90^{\circ} - \dfrac{180^{\circ}}{M} \left( k + \dfrac{1}{2} \right), \quad k = \overline{0, M-1}.
        $$
        Все индивидуальные разности звезд в~одной трапеции осредняют и~получают функцию $f = f (\alpha_i, \delta_k)$ для~$M \cdot N$ фиктивных звезд.
        Осреднение ослабляет случайную ошибку. Характерные размеры трапеции $1^h \times 10^{\circ}$,
        то есть $N = 24, M = 18$ (в~принципе можно использовать другие методы разбиения сферы~--- ECP, HEALPix).
    \item Получение непериодических компонент и~сглаживание соответственно:
        $$
        \varphi_k = \dfrac{1}{N} \sum_{i = 0}^{N-1} f (\alpha_i, \delta_k) \quad k = \overline{0, M-1}; \qquad
        \overline{\varphi_k} = \sum_{\nu = -1}^{1} P_{\nu} \varphi_{k + \nu} \quad k = \overline{1, M - 2} \quad \sum_{\nu = 1}^{1} P_{\nu} = 1.
        $$
        Получили $\Delta \alpha_{\delta}$, $\Delta \delta_{\delta}$, $\Delta \mu_{\delta}$, $\Delta \mu_{\delta}'$ в~виде одномерной таблицы по~$\delta$.
    \item Получение периодических компонент и~сглаживание:
        $$
        f_{\alpha} (\alpha_i, \delta_k) = f (\alpha_i, \delta_k) - \overline{\varphi_k}; \qquad 
        \overline{f_{\alpha}} (\alpha_i, \delta_k) = \sum_{\nu = -m}^{m} h_{\nu} f (\alpha_{i + \nu}, \delta_k), \quad
        i = \overline{m,N - m - 1} \quad k = \overline{0,m - 1}; \quad \sum h_{\nu} = 1.
        $$
        Получили $\Delta \alpha_{\alpha}$, $\Delta \delta_{\alpha}$, $\Delta \mu_{\alpha}$, $\mu_{\alpha}'$ в~виде двумерной таблицы по~$\alpha$ и~$\delta$.
    \item Уравнение яркости. \\
        $\Delta_j = f (\alpha_j, \delta_j) - \overline{f_{\alpha}} (\alpha_j, \delta_j) - \overline{\varphi} (\delta_j)$ (интерполяция). \\
        $\Delta_j = \Delta_0 + \gamma \, (m_j - m_0)$, $j$~--- список звезд.
        По~МНК находим~$\Delta_0$ и~$\gamma$.
\end{enumerate}
\textbf{Недостатки табличного метода}: нет критериев выбора $N$, $M$, самого метода разбиения,
$P_{\nu}$, $h_{\nu}$ (выбор их~суммы единицей~--- эмпирическое правило) и~т.д.
Разное количество звезд попадает, неодинаковые области и~т.д.

\subsubsection{Аналитический метод получения систематических разностей}
\par\noindent Исходные данные~--- индивидуальные разности $\Delta \alpha ,\ \cos \delta$, $\Delta \delta$, $\Delta \mu \, \cos \delta$, $\Delta \mu'$.
Модель:
$$
f(\alpha, \delta, m) = \underbrace{\sum_{j=0}^{J} B_j \, z_j (\alpha, \delta, m)}_\textrm{систем. комп.} + \underbrace{\varepsilon (\alpha, \delta, m)}_\textrm{случ. комп.},
$$
где~$z_j$~--- базисные функции. Минимальное требование~--- \textit{линейная независимость}.
Часто $z_j$ выбирают в~виде произведения ортогональных полиномов, образующих полную систему.
$B_j$~--~коэффициент разложения, $J$~--~старший член разложения.
Задачи определения систем разностей сводится к~определению $J$ и~$B_j$, $j = 0,1,...,J$.

\par\noindent\textbf{Схема определения коэффициентов $B_j$:}
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*,label=\textbf{{\arabic*.}}]
    \item Вычисление векторов, $z_j(\alpha_i, \delta_i, m_i) \quad j = 0, 1,...,\text{ много}$, $j$~--- список звезд
    \item Ортогонализация (процедура Грамма-Шмидта) $z_j \rightarrow \overline z_j (\alpha_i, \delta_i, m_i)$
    \item Вычисление коэффициентов разложения в ортогональном базисе:
        $$
        \overline{B_j} = \frac{(f, \overline{z_j})}{||\overline{z_j}||^2} \quad j = \overline{1, J^*}. \qquad \text{Для } \forall \, J^* \text{ вычисляют остатки: }
        \varepsilon_{J^*} (\alpha_i, \delta_i, m_i) = f(\alpha_i, \delta_i, m_i) - \sum_{j=0}^{J^*} B_j \, z_j (\alpha_i, \delta_i, m_i)
        $$
        Задают вероятность того, что $\varepsilon_{J^*}$ есть случайная величина ($p = 0.95, 0.99$)
        и~с~помощью статистических критериев выявляют справедливость этой гипотезы.
        Таким образом определяется старший член разложения~$J$.
    \item Возвращение в~исходный базис: $\overline B_j \rightarrow B_j$
    \item Получение систематических разностей: $f(\alpha, \delta, m) = \displaystyle\sum_{j=0}^{J} B_j z_j(\alpha, \delta, m)$
\end{enumerate}

\begin{figure}[ht]
    \centering
    \subfloat[$\Delta \alpha$]{\includegraphics[width=0.49\textwidth]{../section04/images/SysDiffA/SysDiffA.png}}
    \subfloat[$\Delta \delta$]{\includegraphics[width=0.49\textwidth]{../section04/images/SysDiffD/SysDiffD.png}}
    \caption{Систематические разности FK5-HIPPARCOS, мсд}
    \label{SystematicErrsFK5}
\end{figure}

% Данный подподраздел копировался с автораспознованием скана~--- возможна мешанина кириллицы и латиницы.
\subsubsection*{Функции "Лежандр-Эрмит-Фурье"}
\noindent В~настоящее время стандартная модель аналитического представления систематических разностей строится с~помощью базисных функций,
образованных произведением полиномов Лежандра, Эрмита и~Фурье.
В~этом случае основное уравнение удобно записать в~следующем виде:
$$
f(\alpha, \delta, m) = \sum_{pnkl} B_{pnkl} \, R_{pnkl} \, H_p(\overline{m}) \, L_n(\overline{\delta})  \, F_{kl}(\alpha) + \varepsilon.
$$
\begin{multicols}{2}
\noindent $\bullet$ В~этой формуле полиномы Эрмита $H_p$ служат для~описания "уравнения яркости". Они вычисляются с~помощью рекуррентного соотношения
\begin{equation*}
\begin{aligned}
    & H_{p+1}(\overline{m}) = \overline{m} H_p(\overline{m}) - p H_{p-1}(\overline{m}), & p = 1,2,... \\
    & H_0 = 1, \; H_1 = \overline{m}, & H_2 = \overline{m}^2 - 1,
\end{aligned}
\end{equation*}
где $\bar{m}$~--- безразмерная звездная величина
$$
\overline{m} = \frac{m - m_0}{\sigma_m},
$$
$m_0$~--- средняя звездная величина звезд сравнения, $\sigma_m$~--- среднеквадратичное уклонение звездных величин от~средней.

\medskip
\par\noindent $\bullet$ Полиномы Лежандра $L_n$ используются для~описания зависимости систематических разностей от~склонения.
Они могут быть вычислены следующим образом:
\begin{equation*}
\begin{alignedat}{1}
& L_{n+1}(\bar{\delta}) = \frac{2n + 1}{n + 1} \, \bar{\delta} \, L_n(\bar{\delta}) - \frac{n}{n+1} L_{n-1} (\bar{\delta}) \\
& n = 0, 1, 2,... \qquad \qquad \qquad L_0 = 1, \; L_1 = \delta,
\end{alignedat}
\end{equation*}% need best aligment
где
\begin{equation*}
\overline{\delta} =
\begin{cases}
    \sin \delta, & |\delta| \le \frac{\pi}{2} \\
    \dfrac{2 \sin \delta}{S_2 - S_1} - \dfrac{S_2 + S_1}{S_2 - S_1}, & \delta_1 \le \delta \le \delta_2,
\end{cases}
\end{equation*}
$$
S_1 = \sin \delta_1; \qquad S_2 = \sin \delta_2.
$$
\end{multicols}

\begin{multicols}{2}
\vspace{\baselineskip}
\par\noindent $\bullet$ Для~описания зависимости систематических разностей от~прямого восхождения используются тригонометрические функции (функции Фурье):
\begin{equation*}
F_{kl}(\alpha) = \left\{
\begin{alignedat}{3}
  	& 1,                     & k = 0, \phantom{k} \; & l = -1, \\
  	& \cos(kl\alpha),        & l = +1, \; & k = 1, 2,... \\
  	& \sin(-kl\alpha), \quad & l = -1, \; & k = 1, 2. \\
\end{alignedat}
\right.
\end{equation*}
$\bullet$ Нормировка базисных функций осуществляется при~помощи множителя:
\begin{equation*}
R_{pnkl} = \frac{\sqrt{2n+1}}{\sqrt{p!}}
\begin{cases}
    1, & k = 0 \\
    \sqrt{2}, & k \neq 0.
\end{cases}
\end{equation*}
\end{multicols}


\vfil
\noindent\textit{Смотрите также:}\\
Bien R., Fricke W., Lederle T., Sclrwan H. Methods for~comparison of~systems of~star positions to be applied in~the~construction of the~FK5.
Veroffen astron. Rechen. Inst.., Heidelberg, N 29, 21 p.
% см. также статью Systematic Differences FK5-FK4: Optimal Representation by O. Titov, E. V. Volkov. Там, кажется, данные для построения 3Д картинок...

\newpage
\subsection{Процедура построения фундаментального каталога}
\noindent Пусть $K_0$~--- улучшаемый фундаментальный каталог.
В двадцатом веке все каталоги улучшались (фундаментальные каталоги основаны на~абсолютных методах).\\
$\alpha_0, \delta_0, \mu_0, \mu_0'$~--- относятся к равноденствию $T_0$ и эпохе  $t_0$.\\
Пусть $\{\alpha, \delta\} = \xi$, $\{\mu, \mu'\} = \mu$.

\medskip
\noindent Наблюдательные каталоги упорядочим по следующей схеме:
$K_1, K_2, ... K_m, K_{m+1}, ..., K_n$~--- каталоги. \\
$T_1, T_2, ... T_m, T_{m+1}, ..., T_n$~--- равноденствия. \\
$\underbrace{t_1, t_2, ... t_m}_\textrm{абсолютные}, \underbrace{t_{m+1}, ..., t_n}_\textrm{относительные}$~--- эпохи.

\medskip
\noindent Все относительные каталоги имеют систему~$K_0$, абсолютные каталоги имеют свои собственные системы.
Все каталоги имеют нуль-пункты каталога~$K_0$.

\medskip
\noindent\textbf{Подготовительная процедура:} \\
$x_i = x_i \, (T_i, t_i) + Pr \, (T_0-T_i) + \mu \, (T_0 - t_i)$,\\
где $i$~--- индекс каталога,\\
$x = \{\alpha, \delta\}$~--- координаты из каталога,\\
$\mu = \{\mu_0, \mu_0'\}$~--- собственные движения из~$K_0$.\\
Этой процедурой все $x_i$~приведены к~равноденствию и~эпохе каталога $K_0$,
используются принятые значения постоянной прецессии (член $Pr$) и~собственного движения из~$K_0$.
Дальнейшая процедура состоит из~следующих этапов: 

\subsubsection*{Этап первый:}
\begin{enumerate}[label=\textbf{{\arabic*.}}]
    \item \textbf{Построение индивидуальных разностей:}\\
        $\Delta x_i = x_i - \xi$. Эти разности представляют собой систематическую ошибку + шум (случайную).
    \item \textbf{Выделение систематических компонент:}\\
        $\Delta i = \left\{\begin{matrix}
            \Delta \alpha_{\alpha} \, \cos \delta & \Delta \delta_{\alpha} \\
            \Delta \alpha_{\delta} \, \cos \delta & \Delta \delta_{\delta} \\
        \end{matrix} \right\}$\\
        Метод: табличный или~аналитический.
    \item \textbf{Перевод всех каталогов на~систему $K_0$:}\\
        $x_i^0 = x_i - \Delta i$
    \item \textbf{Образование новых индивидуальных разностей:} $\Delta x_i^0 = x_i^0 - \xi$. \\
        Эти разности~--- случайные (по~построению) и~они позволяют улучшить~$K_0$ и~$K_i$ в~случайном отношении.
    \item \textbf{Условное уравнение}\\
        $\xi + \Delta \xi = x_i^0 + \Delta x_i + \Delta \mu \, (T_0 - t_i)$, где \\
        $\Delta \xi$~--- искомая поправка координаты каталога $K_0$\\
        $\Delta x_i$~--- поправка координат $i$-го каталога\\
        $\mu$~--- поправка собственного движения $K_0$\\
        Считаем, что $\Delta x_i$~--- случайная величина.
        Тогда отбросив их\footnote{стандартный прием в МНК} можем получить условное уравнение: \\
        $\Delta \xi + \Delta \mu \, (T_0 - t_i) = x_i^0 - \xi = \Delta x_i^0,$ в котором два неизвестных~--- $\Delta \xi$ и $\Delta \mu$.
    \item \textbf{Решение условных уравнений с~весами в~случайном отношении}\\
    Каждому каталогу назначаем вес:
    $$
    P_i \sim \frac{1}{(\sigma_{\infty}^i)^2 + \frac{\sigma_i^2}{n_i}}, \text{ где}
    $$
    $\sigma_i$~--- среднеквадратичная ошибка одного наблюдения в $i$-м каталоге (берется из каталога)\\
    $n_i$~--- среднее число наблюдений в $i$-м каталоге\\
    $\sigma_{\infty}^i$~--- характеристика $i$-го каталога в~систематическом отношении. \textit{Эта величина неизвестна!}\\
    \textbf{Определение $\sigma_{\infty}^i$:}
    \begin{enumerate}[topsep=0pt,leftmargin=*]
        \item Сначала решается условное уравнение с весами: $P_i \sim 1/(\sigma_i^2/n_i))$.\\
            Получаем оценки $\Delta \xi$, $\Delta \mu$ и вычисляем невязки для каждого каталога:\\
            $\Delta e_i = \Delta \xi + \Delta \mu (t_i - T_0) - \Delta x_i^0$
        \item Вычисляем теперь \\
            \begin{minipage}{10.25cm}
                $$
                \sigma^2(m) = \frac{1}{s-1} \sum_{i=1}^{s} \Delta e_i^2(m),
                $$
                где $m$~--- число наблюдений звезд в~$i$-м каталоге,\\
                $\Delta e_i(m)$~--- невязки в $i$-м каталоге для $m$ наблюдений,\\
                $s$~--- общее число невязок $\Delta e_i(m)$.\\
                Подбирают звезды с различным числом наблюдений и получают график вида (см.~рис.~справа).\\
                По этому графику и оценивают $\sigma_{\infty}^i$.
            \end{minipage}\hfill
            \begin{minipage}{6cm}
                \centering \includegraphics[scale=1.0]{../section04/images/SigmaGraph/SigmaGraph.pdf}
            \end{minipage}
        \item Теперь решаем уравнения с учетом $\sigma_{\infty}^i$ и получаем окончательные поправки $\Delta \xi$ и $\Delta \mu$.
    \end{enumerate}
    \item \textbf{Улучшение каталога в случайном отношении}\\
        $\left.\begin{alignedat}{1}
            & \overline{\xi} = \xi + \Delta \xi \\
            & \overline{\mu} = \mu + \Delta \mu
        \end{alignedat} \right| \quad \Rightarrow \quad \ \overline{K_0}$~--- свободный каталог, система \textit{не~улучшена}.\\
        $\overline{x_i} = x_i^0 + \Delta e_i | \quad \Rightarrow \quad \overline{K_i}$~--- улучшенные.
\end{enumerate}

\subsubsection*{Этап второй:}
\noindent Улучшение~$K_0$ в~систематическом отношении.
Теперь наш ряд каталогов выглядит так:

\medskip
\noindent\fbox{\parbox{0.47\textwidth}{
$\overline{K_0}$~--- улучшенный каталог, \\
$\overline{K_1}, \overline{K_2}, ... \overline{K_m}$~--- абсолютные каталоги разных систем, \\
$\overline{K_{m+1}}, ..., \overline{K_n}$~--- относительные каталоги системы $K_0$.}}

\medskip
\noindent Теперь в обработку берут только абсолютные каталоги считая, что их~системы различные:

\begin{enumerate}[noitemsep,label=\textbf{{\arabic*.}}]
    \item \textbf{Индивидуальные разности:} $\overline{x_i} = \overline{x_i} - \overline{\xi}$
    \item \textbf{Получение систематических компонент:}
        $$
        \overline{\Delta_i} = \left\{\begin{matrix}
  	        \Delta \alpha_{\alpha} \, \cos \delta \quad &  \Delta \alpha_{\delta} \, \cos \delta  \\
  	        \Delta \delta_{\alpha} & \Delta \delta_{\delta}
  	    \end{matrix}\right\}
        $$
    \item \textbf{Условные уравнения:}\\ $\Delta \overline{\xi} + \Delta \overline{\mu} \, (t_i - T_0) = \overline{\Delta_i}$ \\
        Решаются по~МНК с~весами.
        Веса назначаются исходя из~анализа методики наблюдений. Итого:
        $$
        \left.\begin{alignedat}{2}
            & \xi = \xi_0 + \Delta \xi + \Delta \overline{\xi} \\
            & \mu = \mu_0 + \Delta \mu + \Delta \overline{\mu} \\
        \end{alignedat}\right\} \,
        \text{~--- каталог с новой системой, но со старыми нуль-пунктами.}
        $$
\end{enumerate}

\subsubsection*{Этап третий:}
\noindent Исправление нуль-пунктов фундаментальных каталогов.
В~двадцатом веке происходило лишь только для~фундаментальных каталогов (и~то не~всех).
Необходимы наблюдения планет и~Солнца:
\begin{enumerate}[label=\textbf{{\arabic*.}}]
    \item \textbf{Наблюдение планет}\\
    Исходные данные: ФК, нуль-пункты которого необходимо уточнить и~теория движения планет и~Земли вокруг Солнца.
        Сложность измерения~--- необходимо определять центры протяженных объектов.
        Составляются эфемериды~("c") и~производятся наблюдения в~системе улучшаемого каталога~("o"). Анализ разностей "o"{}~$-$~"c"{}:
        $$
        \Delta \alpha = \Delta A + \sum_i \dfrac{\partial \alpha}{\partial E_i} \, \Delta E_i + \sum_i \dfrac{\partial \alpha}{\partial E_i'} \, \Delta E_i', \qquad
        \Delta \delta = \Delta D + \sum_i \dfrac{\partial \delta}{\partial E_i} \, \Delta E_i + \sum_i \dfrac{\partial \delta}{\partial E_i'} \, \Delta E_i',
        $$
        где $E_i, E_i'$~--- элементы орбиты планет и~Земли,\\
        $\Delta E_i, \Delta E_i'$~--- их поправки,\\
        $\Delta A, \Delta D$~--- поправки нуль-пунктов каталога.\\
        МНК решение этих уравнений дает $\Delta A, \Delta D$ (астрометрия) и~$\Delta E_i, \Delta E_i'$ (небесная механика).
        Удобно и~хорошо тем, что планеты можно наблюдать на~тех~же инструментах, что и~звезды.
    \newpage
    \item \textbf{Исправление нуль пунктов из~наблюдений Солнца}
        \begin{multicols}{2}
        \par\noindent Переход из~эклиптической в~экваториальную систему координат: 
        $$
        \left[ \begin{matrix}
            \cos \delta \cos \alpha \\ \cos \delta \sin \alpha \\ \sin \delta
        \end{matrix} \right] = \left[ \begin{matrix}
            1 & 0 & 0 \\
            0 & \cos \varepsilon & -\sin \varepsilon \\
            0 & \sin \varepsilon &  \cos \varepsilon \\
        \end{matrix} \right] \, \left[ \begin{matrix}
            \cancelto{1}{\cos \beta} \cos \lambda \\ \cancelto{1}{\cos \beta} \sin \lambda \\ \cancelto{0}{\sin \beta}
        \end{matrix} \right],
        $$
        отсюда имеем уравнения
        $$
        \left. \begin{alignedat}{1}
            & \cos \delta \cos \alpha = \cos \lambda \\
            & \cos \delta \sin \alpha = \cos \varepsilon \sin \lambda \\
            & \sin \delta = \sin \varepsilon \sin \lambda \\
        \end{alignedat} \right\}
        \quad \Rightarrow \quad
        \left\{ \begin{alignedat}{1}
            & \tg \alpha = \tg \lambda \cos \varepsilon \\
            & \tg \delta = \tg \varepsilon \cos \alpha  \\
        \end{alignedat} \right.
        $$
        Представим в~разностном виде первое уравнение из~последней системы\footnote{Подробнее у Подобеда в~"Фундаментальной астрометрии"{}}:
        $$
        \left\{ \begin{alignedat}{1}
            & d \alpha = d \lambda \, \frac{\sin(2 \alpha)}{\sin(2 \lambda)} - d \varepsilon \, \tg \delta \, \cos \alpha \\
            & d \delta = d \lambda \, \sin \varepsilon \, \cos \alpha + d \varepsilon \, \sin \alpha
        \end{alignedat} \right.
        $$
        $$
        \left\{ \begin{alignedat}{1}
            & d \alpha = \alpha_H - \alpha_E + \Delta A = \Delta \alpha + \Delta A \\
            & d \delta = \delta_H - \delta_E + \Delta D = \Delta \delta + \Delta D
        \end{alignedat} \right.
        $$
        $$
        \begin{alignedat}{2}
            & d \lambda = \Delta \lambda & \text{~--- формальная поправка к долготе 0} \\
            & d \varepsilon = \Delta \varepsilon & \text{~--- поправка к принятому значению $\varepsilon$}
        \end{alignedat}
        $$
        Перепишем выражения дифференциалов:
        $$
        \left\{ \begin{alignedat}{1}
            & \Delta \alpha = -\Delta A + \Delta \lambda \, \frac{\sin(2 \alpha)}{\sin(2 \lambda)} - \Delta \varepsilon \, \tg \delta \, \cos \alpha \\
            & \Delta \delta = -\Delta D + \Delta \lambda \, \sin \varepsilon \, \cos \alpha + \Delta \varepsilon \, \sin \alpha
        \end{alignedat} \right.
        $$
        Наблюдают круглый год Солнце в~системе каталога, находя 12~месячных значений $\Delta \alpha$ и~$\Delta \delta$ и~усредняют~их:
        $$
        \begin{alignedat}{1}
            & \overline{\Delta \alpha} = - \Delta A + \overline{\Delta \lambda} \\
            & \overline{\Delta \delta} = - \Delta D
        \end{alignedat}
        $$
        Теперь необходимо найти~$\Delta \lambda$.
        Умножают $\Delta \delta$ на~$\cos \alpha$ и~усредняют (значения берутся из~наблюдений, либо из~моделей, если возможно):
        $$
        [\Delta \delta , \cos \alpha] = \overline{\Delta \lambda} \, \sin \varepsilon \, [\cos^2 \alpha].
        $$
        Итого получают: \hfill\fbox{$\begin{alignedat}{1}
            & \Delta D = - \overline{\Delta \delta} \\
            & \Delta A = - \overline{\Delta \alpha} + \frac{[\Delta \delta \, \cos \alpha]}{\sin \varepsilon \, [\cos^2 \alpha]}.
        \end{alignedat}$}
        \end{multicols}
\end{enumerate}

\subsection*{Заключение}
\noindent Так работала астрометрия до~60-80-х годов XX~века (наземная астрометрия).
Однако, нуль-пункты в~разных определениях отличаются.
Точка весеннего равноденствия движется, в~том числе и~определяется по-разному.
Из~всего этого позже пришли к~концепции невращающегося начала.

\begin{figure}[h]
\centering
\begin{minipage}{0.49\textwidth}
    \subfloat[Поправка точки весеннего равноденствия в FK4]
        {\includegraphics[width=\textwidth]{../section04/images/Correction/Correction.png}}
\end{minipage}
\begin{minipage}{0.49\textwidth}
    \subfloat[Фиктивное движение равноденствия]
        {\includegraphics[width=\textwidth]{../section04/images/VernalEquinox/VernalEquinox.png}}
\end{minipage}
\caption{Нуль-пункты прямых восхождений}
\label{EquinoxPoints}
\end{figure}

\newpage
\noindent Примеры определений точки Весеннего равноденствия в~разных фундаментальных каталогах:\\
\begin{tabular}{ll}
    \textbf{Бессель (1830):}  & \textbf{Tabule Regiomontanae:} $Bess$ \\
    \textbf{Ньюком (1882):}   & $N1 = Bess + 0.016^s$ \\
    \textbf{Карштедт (1931):} & $\text{\textbf{K}} = \text{\textbf{N1}} - 0.050^s$ \\
    \textbf{Морган (1931):}   & $M = N1 - 0.038^s - 0.007^s (T- 1900) / 100$ \\
    \textbf{1937:}            & $\text{\textbf{GC}} =  \text{\textbf{N1}} - 0.040^s$ 
    $\text{\textbf{FK3}} = \text{\textbf{N1}} - 0.050^s$ \\
    \textbf{Фрике (1931):}    & $F = FK4 + 0.035^s + 0.085^s (T- 1950.0) / 100$ \\
\end{tabular}

\begin{table}[h]
\centering
\caption{Фундаментальные каталоги звезд}
\begin{tabular}{|c|c|c|c|c|c|c|}
    \hline     
    Название & Автор, год             & Ср. эпоха & Р/д         & N звезд & Нуль-пункт RA   & Примечания \\ \hline 
    TR       & Бессель, 1830          & 1800      & 1815, 1825  & 36      & Bess.           & Первый ФК  \\ \hline 
    N1       & Ньюком, 1872           & 1840      & 1700(5)1800 & 32      & N1=Bess.+0.016s &            \\ \hline 
    N2       & Ньюком, 1892           & 1865      & 1875, 1900  & 1257    & N1=Bess.+0.016s & САП (1896) \\ \hline 
    PGC      & Л. Босс, 1910          & 1870      & 1900        & 6188    & N1              &            \\ \hline 
    GC       & Л. Босс, Б. Босс, 1937 & 1900      & 1950        & 33342   & N1-0.040s       &            \\ \hline 
    N30      & Х. Морган, 1952        & 1930      & 1950        & 5268    & N1-0.040s       &            \\ \hline 
\end{tabular} 
\end{table}

\begin{table}[h]
\centering
\caption{Фундаментальные каталоги немецкой школы (серия FK, являлись стандартными)}
\begin{tabular}{|c|c|c|c|c|c|>{\raggedright\arraybackslash}m{2cm}|}
    \hline
    Название & Автор, год         & Ср. эпоха  & Р/д          & N звезд   & Нуль-пункт RA  & Примечания         \\ \hline 
    FC       & Ауверс, 1875       & 1865       & 1875         & 539       & N1             &                    \\ \hline 
    NFK      & Петерс, 1900       & 1870       & 1870, 1900   & 925       & N1             &                    \\ \hline 
    FK3      & Копп, 1937         & 1900       & 1950         & 1535      & N1-0.050s      & Стандарт 1940-1964 \\ \hline 
    FK4      & Коппф, Фрике, 1964 & 1935       & 1950, 1975   & 1535      & N1-0.050s      & Стандарт 1940-1989 \\ \hline 
    FK5      & Фрике и др., 1989  & 1944, 1955 & 1950, 2000   & 1535+3117 & N1-0.050+0.035 & Стандарт 1989-1998 \\ \hline 
\end{tabular} 
\end{table}

\begin{table}[h]
\centering
\caption{Фотографические каталоги (распространители фундаментальных систем)}
\begin{tabular}{|c|c|c|c|c|}
    \hline     
    Название & Опорный каталог & Год выпуска & Число звезд  & Примечания         \\ \hline 
    AGK1     & FC, NFK          & 1890-1910   & $144 \, 218$ & Северное полушарие \\ \hline 
    AGK2     & FK3              & 1928-1932   & $188 \, 000$ & Северное полушарие \\ \hline 
    AGK3     & FK4              & 1955-1971   & $188 \, 000$ & Северное полушарие \\ \hline 
    SAO      & FK4              & 1968        & $260 \, 000$ & Все небо           \\ \hline 
    PPM      & FK5              & 1993        & $378 \, 910$ & Все небо           \\ \hline 
\end{tabular} 
\caption*{Точности 200 мсд, 4 мсд/год}
\end{table}

\end{document}
