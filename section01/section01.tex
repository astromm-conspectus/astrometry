\documentclass[../main/main.tex]{subfiles}

\begin{document}
\begin{quote}
\begin{flushright}
\textit{Астрометрия --- это то, чем занимаются астрометристы.}
\end{flushright}
\end{quote}

\section{Введение}
\noindent\textbf{Астрометрия}~--- раздел астрономии, изучающий геометрические, кинематические и~динамические характеристики небесных тел.

\medskip
\noindent\textbf{Астрометрия}~--- наука, занимающаяся построением пространственно-временных систем отсчета и~измерением координат небесных тел в~этих системах.

\medskip
\noindent Второе определение отражает в~себе современный взгляд на~астрометрию.

\noindent \paragraph{Основные задачи, решаемые астрометрией:}
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*]
    \item Определение астрометрических параметров: прямое восхождение~($\alpha$), склонение~($\delta$),\\
        собственное движение~($\dot{\alpha}, \, \dot{\delta}$), параллакс~($\pi$), лучевая скорость~($\upsilon_r$).
    \item Построение системы отчета на~небе и~на~Земле.
    \item Постоянный мониторинг их~взаимной ориентации.
\end{enumerate}
\medskip
\textit{Система отчета~-- инструмент для~других разделов.}

\medskip
\noindent\textbf{Инструменты астрометрии\footnote{В~скобках указаны астрометрические величины, определяемые данным инструментом}:}\\
\begin{minipage}[t]{7cm}
    \subparagraph{Классические:}
    \begin{itemize}[noitemsep,topsep=0pt,leftmargin=*]
        \item Пассажный инструмент ($\alpha$)
        \item Меридианный круг ($\delta$)
        \item Вертикальный круг ($\alpha, \delta$)
        \item Универсальный инструмент ($\alpha, \delta, A$)
        \item Астролябия ($\varphi$)
        \item Зенитный телескоп/труба ($\varphi$)
    \end{itemize}
\end{minipage}
\hfill
\begin{minipage}[t]{10.5cm}
    \subparagraph{Фотографические:}
    \begin{itemize}[noitemsep,topsep=0pt,leftmargin=*]
        \item Астрографы ($\alpha, \delta, \pi$)
        \item Спутниковые камеры ($\alpha, \delta$)
    \end{itemize}
    \subparagraph{Современные:}
    \begin{itemize}[noitemsep,topsep=0pt,leftmargin=*]
        \item Радиоинтерферометры ($\alpha, \delta$, параметры ориентации системы)
        \item Лазерные дальномеры и~радиолокаторы ($\tau$)
        \item Глобальные навигационные спутниковые системы
        \item Космические телескопы
        \item Стандарты времени и~частоты
    \end{itemize}
\end{minipage}

\subsection{Основные понятия}
\noindent\textbf{Reference System (RS), Пространственно-временная система координат (ПВСК)}~---
теоретическая концепция пространственно-временных координат,
моделей и~стандартов, которая позволяет измерять положения и~движения объектов в~пространстве и~времени.

\medskip
\noindent Для задания ПВСК необходимы:
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*]
    \item Теория пространства-времени (классическая теория или ОТО)
    \item Система координат и~аппарат преобразования координат
    \item Модели физических явлений\footnote{движение Земли по~орбите, вращение, прецессии, нутации и~т.д.}, влияющих на~измерения
    \item Значения астрономических постоянных (параметры моделей)
\end{enumerate}
Иначе говоря, ПВСК~--- набор договоренностей и~основных принципов построения системы координат,
который включает выбор точки отсчета, направления осей, физических моделей, констант и~аппаратов преобразования.

\medskip
\noindent\textbf{Reference Frame (RF), Пространственно-временная система отсчета (ПВСО)}~--- практическая реализация концепции ПВСК,
созданная с~помощью создания шкал времени и~каталогов опорных источников с~известными положениями и~собственными движениями.

\medskip
\noindent Для~реализации временной координаты нужны стандартные шкалы времени,
а~для реализации пространственных координат соответствующие системы отсчета.
Стандартные шкалы времени обычно основаны на~некотором приближенном периодическом природном процессе
(вращение Земли, монохроматическое излучение определенных атомов).
Стандартные пространственные системы отсчета основаны на~некоторой группе астрономических объектов,
координаты которых считают известными (в~данный момент времени).
Если это~необходимо, учитываются изменения их~координат со~временем,
в~противном случае координаты объектов считаются постоянным (например, неподвижные квазары на~небе).
\textit{Есть три способа реализации}: геометрический, кинематический, динамический.

\medskip
\noindent ICRS~--- международная небесная система координат.
Центр~--- барицентр Солнечной системы.
Оси зафиксированы в~пространстве относительно внегалактических радиоисточников (направление выбрано по~осям в~FK5).
Шкала времени~--- TCB.

\subsection{Методология астрометрии}
\begin{multicols}{2}
    \noindent RS = физическая теория + модель
    \\ RF = система отсчета
    \\ I = инструмент
    \\ RS $\rightarrow$ эфемериды "calculatum"\, = "c"{}
    \\ I, RF $\rightarrow$ наблюдения "observatum"\, = "o"{}
    \\ "o"{} $-$ "c"{} = $f(\text{RS, RF, I, } \Delta \text{RS, } \Delta \text{RF, } \Delta \text{I}) \rightarrow \text{min}$
    \\ $\Delta \text{RS}$ --- улучшение модели/физической теории
    \\ $\Delta \text{RF}$ --- улучшение (уточнение) каталогов
    \\ $\Delta \text{I}$ --- улучшение инструмента
\end{multicols}
$$
\left. \begin{alignedat}{1}
& \text{$\text{RS}_n$: Ньютоновская теория тяготения $\rightarrow$ ОТО}\\
& \text{$\text{RF}_n$: FK3 $\rightarrow$ FK4 $\rightarrow$ FK5 $\rightarrow$ ICRF $\rightarrow$ HCRF}\\
& \text{$\text{I}_n$: меридианные круги $\rightarrow$ РСДБ $\rightarrow$ ГНСС}\\
\end{alignedat} \right\} \text{примеры развития.}
$$

\begin{wrapfigure}{r}{9cm}
    \centering
    \includegraphics[scale=1]{../section01/images/Axes/Axes.pdf}
    \label{Axes}
    \caption{Шкалы идеального и~реального инструментов}
    \vspace{-1.8cm}
\end{wrapfigure}

\noindent\textbf{Инструмент}\\
\textbf{Идеальный:} система $XYZ$, равномерная временная шкала.
Инструмент~-- идеальная математическая абстракция в~модельной (конечном числе моделей) среде.
Идеальная пространственная система координат создает local~RS.\\
\textbf{Реальный:} масштаб не~постоянный, среда реальная и~идеально не~описывается. Создает local~RF.

\medskip
\noindent Рассмотрим вектора координат тела:
$$    
\vec{\rho} = \left[ 
\begin{array}{c}
    x \\ y \\ z \\ t \\
\end{array} \right] \text{ --- ("c"\,) идеальный,}
$$
$$
\vec{\rho'} = \left[ 
\begin{array}{c}
    x' \\ y' \\ z' \\ t' \\
\end{array} \right] \text{ --- ("o"\,) реальный,}
$$
$$
\Delta \vec{\rho} = \vec{\rho'} - \vec{\rho}, \quad \Delta \vec{\rho} = \Delta \vec{\rho_r} + \Delta \vec{\rho_s}.
$$
$\Delta \vec{\rho_r}$ --- случайные ошибки.
Характеризуются статистическими методами.
Ошибка среднего убывает с~ростом числа измерений~($n$).
Обычно имеет смысл лишь до~$n \le 100$: $\sigma_n^2 = \sigma_{\infty}^2 + \sigma_0^2 / n$ из-за наличия неучтенных систематических ошибок.\\
$\Delta \vec{\rho_s}$~--- систематические ошибки. Влияние реальных явлений, отсутствующих в~моделях, некорректность методов обработки, дефекты инструментов. \textit{Например}: неидеальность ориентации инструмента; ошибки шкал.

\noindent $\vec{\rho'} = M \vec{\rho}$~-- редукционное уравнение $(\ast)$. $M$: $\quad m_{ij} = \sum_k \mu_k^{ij}$, $i=1,..,4$.

\noindent Для~редукции используются разные модели, в~частности:
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*]
    \item Формулы Майера и~Бесселя (для~учета систематических ошибок в~меридианной астрономии),
    \item Модели пластинок (в фотографической астрометрии),
    \item Модель задержки РСДБ.
    \item Модель астрометрических параллаксов (HIPPARCOS).
\end{enumerate}

\begin{wrapfigure}{l}{6cm}
    \centering
    \includegraphics[scale=1]{../section01/images/AxesRotation/AxesRotation.pdf}
    \label{AxesRotation}
    \caption{Повороты системы координат}
    \vspace{-2cm}
\end{wrapfigure}
\noindent Запишем матрицы поворота координатный осей и~их приближения в~случае малых углов ($\alpha, \beta, \gamma \ll 1$):
$$
R_x(\alpha) = \left( \begin{matrix}
  1 &            0 &            0 \\
  0 &  \cos \alpha &  \sin \alpha \\
  0 & -\sin \alpha &  \cos \alpha \\
\end{matrix} \right),
\quad
R_x(\alpha) \approx \left( \begin{matrix}
  1 &       0 &       0 \\
  0 &       1 &  \alpha \\
  0 & -\alpha &       1 \\
\end{matrix} \right),
$$
$$
R_y(\beta) = \left( \begin{matrix}
   \cos \beta  & 0 &  -\sin \beta  \\
   0           & 1 &             0 \\
   \sin \beta  & 0 &  \cos  \beta  \\
\end{matrix} \right),
\quad 
R_y(\beta) \approx \left( \begin{matrix}
   1      & 0 &  -\beta \\
   0      & 1 &       0 \\
   \beta  & 0 &       1 \\
\end{matrix} \right),
$$
$$
R_z(\gamma) = \left( \begin{matrix}
  \cos \gamma &  \sin \gamma & 0 \\
 -\sin \gamma &  \cos \gamma & 0 \\
  0           &            0 & 1 \\
\end{matrix} \right), 
\quad 
R_z(\gamma) \approx \left( \begin{matrix}
  1      & \gamma  & 0 \\
 -\gamma &  1      & 0 \\
  0      &  0      & 1 \\
\end{matrix} \right),
$$
\begin{equation*}
R(\alpha, \beta, \gamma) = R_x(\alpha) \, R_y(\beta) \, R_z(\gamma) \approx \left( \begin{matrix}
  1       &  \gamma &  -\beta \\
  -\gamma &       1 &  \alpha \\
    \beta & -\alpha &       1 \\
\end{matrix} \right).
\end{equation*}

Рассмотрим "o"{}, "c"{} вектора и~их связь:
$$
\vec{r'} = R(\alpha, \beta, \gamma) \, \vec{r};
\quad
\vec{r'} = \left( 
\begin{array}{c}
    x' \\ y' \\ z'
\end{array} \right),
\quad
\vec{r} = \left( 
\begin{array}{c}
x \\ y \\ z
\end{array} \right),
\quad
\Delta \vec{r} = \vec{r'} - \vec{r} = \left( R(\alpha, \beta, \gamma) - E \right) \, \vec{r}
$$
Представим единичный вектор направления (вектор на~единичной сфере) через угловые переменные~$a$ и~$d$ (сделаем замену координат):
$$
\vec{r} = 
\begin{pmatrix}
    x \\ y \\ z
\end{pmatrix}
=
\begin{pmatrix}
    \cos d \cos a \\ \cos d \sin a \\ \sin d
\end{pmatrix},
\quad
\vec{r'} = 
\begin{pmatrix}
    x \\ y \\ z
\end{pmatrix}
=
\begin{pmatrix}
    \cos d' \cos a' \\ \cos d' \sin a' \\ \sin d'
\end{pmatrix},
$$
Разность~$\vec{r}$ и~$\vec{r'}$ представим линейным приближением, и~тогда найдем
$$
\begin{alignedat}{1}
& \Delta a \cos d = - \gamma \cos d + \beta \sin d \sin a + \alpha \sin d \cos a, \\
& \Delta d = \beta \cos a - \alpha \sin a.
\end{alignedat}
$$
Без~редукции наблюдения довольно плохи.
Измеряя координаты опорного объекта и~сравнивая измерения с~каталогами, получаем редукционную матрицу.
Если в~каталоге были ошибки, то~они будут и~в~редукционной матрице.

% Это подподраздел требует уточнений и более подробного изложения.
\subsubsection{Измерение времени}
\noindent\textbf{Часы}~--- генератор времени и~частоты.

\medskip
\noindent\textbf{Стандарты времени и~частоты (СВиЧ)}
\begin{itemize}[noitemsep,topsep=0pt,leftmargin=*]
    \item \textbf{Идеальная шкала:} $f = \dfrac{2 \pi}{P}, \quad P = \text{const}, \quad T = f \cdot t$~--- идеальный периодический процесс.
    \item \textbf{Реальная шкала:}  $P = P(t)$~--- квазипериодический процесс.\\
        \noindent $f(t) = \dfrac{2 \pi}{P(t)}, \quad T' = \int\limits_0^t f(t') \, dt'$.
        $T$, $T'$~--- временные координаты, $t$~--- время.
\end{itemize}

% Уточнить: не сходится размерность у хода часов.
% По последней формуле во втором пункте размерность омега должна быть Гц, но по определению она безразмерная
% (выше временные координаты были заведены как безразмерные величины).
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*]
    \item Поправка часов: $U = T - T'$
    \item Ход часов: $\omega = \dfrac{U(T') - U(T)}{T' - T} = \text{const}$;\\
        $U(t) = U(t_0) + \omega \, (t - t_0)$; $\dot{U} = \omega$, $\omega = s + n$~--- систематическая и~случайная компоненты.
    \item $\Delta_i = \omega_{i+1} - \omega_i$ ($i$~--- например, в~разные сутки)~--- разность хода. Освобождена от~системной ошибки.\\
        $\delta = \sqrt{\dfrac{\sum_{i=1}^n \Delta_i^2}{n-1}}$~--- вариация суточного хода. % $\delta \le 0^s.001$: почему секунды? Величина без
\end{enumerate}

\begin{table}[ht]
\centering
\begin{tabular}{l|c|c}
    \textbf{Вариация суточного хода} $\delta$ для ...  & сек/сутки                 & сек/сек                  \\
    Механические часы                                  & $10^{0}   \div 10^{1}   $ & $10^{-4}  \div 10^{-5} $ \\
    Маятниковые                                        & $10^{-3}  \div 10^{-4}  $ & $10^{-8}  \div 10^{-9} $ \\
    Кварцевые часы                                     & $10^{-6}  \div 10^{-7}  $ & $10^{-11} \div 10^{-12}$ \\
    Атомные                                            & $10^{-10} \div 10^{-11} $ & $10^{-15} \div 10^{-16}$ \\ 
\end{tabular} 
\caption{Вариации суточного хода~$\delta$ для~разных часов}
\label{variationTime}
\end{table}

\medskip
\noindent\textbf{Атомные часы}: $\nu_0 = 9\,192\,631\,770$ Гц. $\nu \ne \nu_0$,
расхождение за~год $\Delta T = \int\limits_0^t \Delta \nu \, dt' = 1.5 \cdot 10^{-6} \text{ с}$.

\noindent\textbf{Нестабильность. Дисперсия Аллана}\\
$\nu (t)$~--- мгновенная частота.
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*]
    \item $x_k = {\displaystyle \int\limits_{t_k}^{t_k + \tau} \dfrac{\nu(t) - \nu_0}{\nu_0} \, dt} = \tau_k' - \tau$
    \item $y_k = \dfrac{x_{k+1} - x_k}{\tau} \approx \dfrac{d}{d \tau} (x_k(\tau)) = \dfrac{\nu(t_k+\tau)}{\nu_0} - 1 = \dfrac{\nu(t_k+\tau) - \nu_0}{\nu_0}$
    \item Дисперсия Аллана: $\sigma_y^2 (\tau) = \dfrac{1}{2} <(\bar{y}_{k+1} - \bar{y}_{k})^2>$,
        \quad $\bar{y}_{k} = \dfrac{1}{\tau} {\displaystyle \int\limits_{t_k}^{t_{k+1}} y(t) \, dt}$, $y(t)$~--- интерполяция сетчатой функции. % Уточнить!
\end{enumerate}

\begin{figure}[ht]
\begin{center}
\subfloat[Нестабильная, но точная]{
\includegraphics[width=0.49\textwidth]{../section01/images/StabilityAccuracyA/StabilityAccuracyA.pdf}}
\hfill
\subfloat[Стабильная, но неточная]{
\includegraphics[width=0.49\textwidth]{../section01/images/StabilityAccuracyB/StabilityAccuracyB.pdf}
}

\subfloat[Нестабильная, неточная]{
\includegraphics[width=0.49\textwidth]{../section01/images/StabilityAccuracyC/StabilityAccuracyC.pdf}}
\hfill
\subfloat[Стабильная, точная]{
\includegraphics[width=0.49\textwidth]{../section01/images/StabilityAccuracyD/StabilityAccuracyD.pdf}
}
\end{center}
\caption{Стабильность и точность}
\label{stableAccuracy}
\end{figure}
\noindent\underline{Стабильность важнее, чем точность.}

\medskip
\noindent\textit{Типы атомных часовых стандартов:} цезиевые, водородные, рубидиевые, ртутные.
Цезиевые и~водородные~--- основные базовые стандарты, а~рубидиевые~--- компактные и~легкие (используются для~сличения стандартов).

\noindent TAI~--- координатное время (с~точки зрения ОТО).\\
\textbf{Единица времени TAI}~--- промежуток времени, за~который совершается~$9\,192\,631\,770$ колебаний,
соответствующих частоте излучения атома~Cs-133 при~резонансном переходе между энергетическими уровнями
сверхтонкой структуры основного состояния при~отсутствии внешних магнитных полей на~уровне моря.

\newpage
\noindent\texttt{<место для более полной исторической справки про шкалы времени>}
\paragraph{Классические шкалы времени}
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*]
    \item UT0~--- всемирное время, \textit{среднее солнечное на~мгновенном Гринвичском меридиане.}
    \item UT1 = UT0 $- \, \Delta \lambda$, где $\Delta \lambda = (x \, \sin \lambda + y \, \cos \lambda) \tg \varphi$~--- \textit{поправка из-за движения полюса.}
    \item UT2 = UT1 $+ \, \Delta T_s$, где
        $ \Delta T_s = 0^{s}.0220 \, \sin(2 \pi \theta) - 0^{s}.0120 \, \cos(2 \pi \theta) - 0^{s}.0060 \, \sin(4 \pi \theta) + 0^{s}.0070 \, \cos(4 \pi \theta)$,
        а~\mbox{$\theta = 2000.0 + (\text{JD} - 2451544.533) / 365.2422$}, JD~--- юлианская дата наблюдения.
        Это \textit{поправка на~сезонные вариации.}
    \item UT1R = UT1 $+ \, \Delta$, где~$\Delta$ -- \textit{поправка на~приливы.}
  % \item S = ???
    \item UTC~--- всемирное координированное время. Атомное время, аппроксимирующее UT1: $|\text{UTC} - \text{UT1}| \le 0^{s}.9$.
        Фактически, UTC~=~TAI~+~T, где T~-- секунды координации. Добавляются (и~вычитаются) при~накоплении большего расхождения (больше $0^{s}.9$).
    \item ET = UT $+ \, \Delta T$~--- эфемеридное время. 
        В~качестве UT может браться UT0, UT1 и~т.д., ET будет индексироваться соответственно.
        Из~предположения, что теория движения планет и~спутников (в~Ньютоновском приближении) идеальна,
        и~строится данная равномерная шкала времени.
        $\Delta T$~--- поправка на~вековое замедление вращения Земли.
\end{enumerate}
\paragraph{Релятивистские шкалы времени}
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*]
    \item TDT = TAI $+ \, 32^{s}.184$~--- земное динамическое время. \textit{Добавка к~TAI для~сохранения эфемеридного времени.}
    \item TDB = TDT $+ \, 0^{s}.0017 \sin E$~--- барицентрическое динамическое время, где~$E$~--- эксцентрическая аномалия Земли.
    \item TT = TDT.
    \item TCG = TT $+ \, L_g (\text{JD} - 2443144.5) / 86400)$~--- геоцентрическое координатное время, где~$L_g =  6.969291 \cdot 10^{-10}$~с, JD~--- юлианская дата.
    \item TCB = TCG $+ \, \Delta$~--- барицентрическое координатное время, где~$\Delta$~--- сложная поправка за~движение Земли по~орбите.
\end{enumerate}

\subsection{"ICRS, ICRF и все-все-все"{}}
\noindent\texttt{<место для энциклопедической информации этого раздела, (временно) см. дополнительные материалы>}
\end{document}
