\documentclass[../main/main.tex]{subfiles}

\begin{document}
\section{Меридианная астрометрия}
\noindent Основная задача, решаемая меридианной астрометрией (она же~фундаментальная астрометрия)~---
измерение экваториальных координат абсолютным и~относительным методами при~наблюдениях в~меридиане.

\subsubsection*{Идеальный инструмент меридианной астрометрии}
\twoitemscolumn
{
Направление на~светило в~горизонтальной\\ системе координат ($h$, $A$, $z = 90^{\circ} - h$):
$$
\vec{r}_H = \left[ \begin{matrix}
    \phantom{-} \cos h \cos A \\ - \cos h \sin A \\ \sin h
\end{matrix} \right].
$$
}
{
Направление на~светило в~экваториальной\\ системе координат ($\delta$, $t$, $t = S - \alpha$):
$$
\vec{r}_E = \left[ \begin{matrix}
    \phantom{-} \cos \delta \cos t \\ - \cos \delta \sin t \\ \sin \delta
\end{matrix} \right].
$$
}
\begin{figure}[h]
    \centering
    \subfloat[Горизонтальная система координат]{
    \includegraphics[width=0.49\textwidth]{../section02/images/HorCoord/HorCoord.pdf}}\hfill
    \subfloat[Экваториальная система координат]{
    \includegraphics[width=0.49\textwidth]{../section02/images/EquCoord/EquCoord.pdf}}\hfill
    \caption{Системы координат}
    \label{HorEquCoord}
\end{figure}
\noindent Постановка задачи в идеальном случае: в~момент~$S$ по~звездному времени определить склонение $\delta$.
Наблюдаем только в~меридиане. Осуществим переход между системами координат.
Для~этого повернем систему координат вокруг оси~$y$ на~угол $\theta_y = - (90^{\circ} - \varphi)$,
то~есть $\vec{r}_E = R_r(\varphi) \, \vec{r}_H$.
Так~как наблюдения происходят в~меридиане, то~возможны два случая: $A = 0^{\circ}$ $(t = 0^h) $ и~$A = 180^{\circ}$ $(t = 12^h)$.
\twoitemscolumn
{
Рассмотрим первый случай, когда $A = 0^{\circ}$ и~$t = 0^h$:
\begin{equation*}
    \left( \begin{matrix}
        \cos \delta \\ 0 \\ \sin \delta
    \end{matrix} \right) = \left( \begin{matrix}
        \sin \varphi  & 0 & \cos \varphi \\
        0             & 1 & 0            \\
        -\cos \varphi & 0 & \sin \varphi \\
    \end{matrix} \right) \, \left( \begin{matrix}
        \sin z \\ 0 \\ \cos z
    \end{matrix} \right)
\end{equation*}
\begin{equation*}
    \left\{
    \begin{alignedat}{1}
        & \cos \delta = \cos (\varphi - z) \\
        & \sin \delta = \sin (\varphi - z) \\
    \end{alignedat} \right.
\end{equation*}
\begin{equation*}
    \left[ \begin{alignedat}{1}
        & \delta = \varphi - z \text{ (к югу от Z)}, \\
        & \delta = \varphi + z \text{ (к северу от Z)}.
    \end{alignedat} \right.
\end{equation*}
}{
Рассмотрим второй случай, когда $A = 180^{\circ}$ и~$t = 12^h$:
\begin{equation*}
    \left( \begin{matrix}
        - \cos \delta \\ 0 \\ \sin \delta
    \end{matrix} \right) = \left( \begin{matrix}
        \sin \varphi  & 0 & \cos \varphi \\
        0             & 1 & 0            \\
        -\cos \varphi & 0 & \sin \varphi \\
    \end{matrix} \right) \, \left( \begin{matrix}
        - \sin z \\ 0 \\ \cos z
    \end{matrix} \right)
\end{equation*}
\begin{equation*}
    \left\{
    \begin{alignedat}{1}
        & \cos \delta = \cos (180^{\circ} - (\varphi + z)) \\
        & \sin \delta = \sin (180^{\circ} - (\varphi + z)) \\
    \end{alignedat} \right.
\end{equation*}
\begin{equation*}
    \left[ \begin{alignedat}{1}
        & \delta = 180^{\circ} - (\varphi + z) \text{ (к северу от Z)}, \\
        & \delta = z - 180^{\circ} - \varphi \text{ (к югу от Z)}.
    \end{alignedat} \right.
\end{equation*}
}
\subsubsection*{Реальный инструмент меридианной астрометрии}
\noindent
\begin{itemize}[noitemsep,topsep=0pt,leftmargin=*]
    \item Пассажный инструмент (ПИ)~--- для~определения времени прохождения, в~конечном счете~--- $\alpha$.
    \item Вертикальный круг (ВК)~--- для определения высот/зенитных расстояний, в~конечном счете~--- $\delta$.
    \item Меридианный круг (МК)~--- универсал ($\alpha$, $\delta$).
\end{itemize}
\noindent
\textbf{Визирная линия}~--- отрезок прямой, соединяющий вторую Гауссову точку объектива с~крестом нитей окуляра микрометра.

\paragraph{Возможные ошибки}
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*]
    \item Наклон горизонтальной линии, $i > 0^{\circ}$, исправляется поворотом $\theta_x = -i$.
    \item Ошибка азимута, $k > 0^{\circ}$, исправляется поворотом $\theta_z = -k$.
    \item Коллимация~--- отклонение от~$90^{\circ}$ угла между визирной линией и~горизонтальной осью инструмента.
        С~коллимацией борются перекладкой инструмента. $\theta_z = c \, \csc \delta$. 
\end{enumerate}
Также ошибки: место зенита (ошибка направления на зенит), ошибки деления вертикального круга, гнутие инструмента, рефракция.

\subsection{Редукционные уравнения}
\noindent Переход между системами координат в~идеальном случае $\vec{r}_E = R(\varepsilon) \, \vec{r}_H$.
Но из-за необходимости редукций уравнение перехода будет $\vec{r'}_E = R(\varphi) \, M(\theta_x, \theta_y, \theta_z) \, \vec{r}_H$.
Рассмотрим разность:
$$
\Delta \vec{r}_E = \vec{r'}_E - \vec{r}_E = R(\varphi) (M(\theta_x, \theta_y, \theta_z) - E) \, \vec{r}_H,
$$
так как углы $\theta_x$, $\theta_y$, $\theta_z$ малы ($<1'$), запишем приближение:
$$
\Delta \vec{r}_E = \left( \begin{matrix}
    \sin \varphi   & 0 & \cos \varphi \\
    0              & 1 & 0            \\
    - \cos \varphi & 0 & \sin \varphi \\
\end{matrix} \right)
\, 
\left( \begin{matrix}
    0 & \theta_z & - \theta_y  \\
    - \theta_z  & 0 & \theta_x \\
    \theta_y & - \theta_x & 0  \\
\end{matrix} \right) \, \left( \begin{matrix}
        \sin z \\ 0 \\ \cos z
    \end{matrix} \right).
$$
С~другой стороны, можно посчитать дифференциал~$\Delta \vec{r}_E$ и~заменить
дифференциалы аргументов на~конечные приращения в~меридиане ($t = 0^h$), тогда:
$$
\Delta \vec{r}_E = \left. \left( \begin{matrix}
    - \Delta \delta \sin \delta \cos t - \Delta t \cos \delta \sin t \\
    \phantom{-} \Delta \delta \sin \delta \sin t - \Delta t \cos \delta \cos t \\
    \Delta \delta \cos \delta \\
\end{matrix} \right) \right\vert_{t = 0} = \left( \begin{matrix}
    - \Delta \delta \sin \delta \\
    - \Delta t \cos \delta \\
    \Delta \delta \cos \delta \\
\end{matrix} \right).
$$
Итого получим
$$
\left( \begin{matrix}
    - \Delta \delta \sin \delta \\
    - \Delta t \cos \delta \\
    \phantom{-} \Delta \delta \cos \delta \\
\end{matrix} \right) = \left( \begin{matrix}
    \theta_y \, \cos \varphi \sin z - \theta_y \, \sin \varphi \cos z \\
    - \theta_z \, \sin z + \theta_x \, \cos z \\
    \theta_y \, \sin \varphi \sin z + \theta_y \, \cos \varphi \cos z \\
\end{matrix} \right).
$$
Возьмем второе координатное уравнение: $\Delta t \cos \delta = \theta_z \sin z - \theta_x \cos z$.
Из~соотношения связи часового угла и~прямого восхождения получим $\Delta t = t' - t = \alpha - \alpha'$.
Учтя, что $\alpha' = T + U$ ($T$~--- показания хронометра, $U$~--- его поправка), получим:
$$
\alpha = T + U + i \, \underbrace{\cos z \sec \delta}_\textrm{I} + k \, \underbrace{\sin z \sec \delta}_\textrm{K} + c \, \underbrace{\sec \delta}_\textrm{C}.
$$
Это формула Майера.
Исторически оптимизирована для~ручных вычислений.

\medskip
\noindent
\begin{minipage}{9cm}
\centering
\includegraphics{../section02/images/BesCoord/BesCoord.pdf}
\end{minipage}\hfill
\begin{minipage}{8.5cm}
 % Уточнить про W'. Может строить написать подробнее.
\noindent Теперь получим формулу Бесселя.
Точка $W'$~--- проекция западного конца горизонтальной оси инструмента на~небесную сферу.
Ее~координаты в~экваториальной системе $\delta = n$, $t = 90^{\circ} - m$,
но~в~горизонтальной системе координат $h = i$ ($i$~--- наклон), $A = 90^{\circ} - k$ ($k$~--- ошибка азимута).
Переход из~экваториальной в~горизонтальную систему координат:
$$
\left[ \begin{matrix}
    \phantom{-} \cos h \cos A \\
    - \cos h \sin A \\ 
    \sin h \\
\end{matrix} \right] = R_2 (\theta_2) \, \left[ \begin{matrix}
    \phantom{-} \cos \delta \cos t \\
    - \cos \delta \sin t \\ 
    \sin \delta \\
\end{matrix} \right],
$$
где
$$
R_2 (\theta_2) = \left[ \begin{matrix}
    \cos \theta_2 & 0 & - \sin \theta_2 \\
    0 & 1 & 0 \\
    \sin \theta_2 & 0 & \cos \theta_2 \\
\end{matrix} \right], \quad \theta_2 = 90^{\circ} - \varphi.
$$
\end{minipage}

\medskip
\noindent Так как $h = i$, $A = 90^{\circ} - k$, $\delta = n$, $t = 90^{\circ} - m$, то
$$
\left[ \begin{matrix}
    \phantom{-} \cos i \sin k \\
    - \cos i \cos k \\ 
    \sin i \\
\end{matrix} \right] = R_2 (\theta_2) \, \left[ \begin{matrix}
    \phantom{-} \cos n \sin m \\
    - \cos n \cos m \\ 
    \sin n \\
\end{matrix} \right],
$$
а~так как углы $m$, $n$, $i$, $k$ очень маленькие, а~$\theta_2 = 90^{\circ} - \varphi$, то
$$
\left[ \begin{matrix}
    k \\  - 1 \\ i \\
\end{matrix} \right] = \left[ \begin{matrix}
    \sin \varphi & 0 & - \cos \varphi \\
    0 & 1 & 0 \\
    \cos \varphi & 0 & \sin \varphi \\
\end{matrix} \right] \,
\left[ \begin{matrix}
    m \\ -1 \\ n \\
\end{matrix} \right]
\quad \Rightarrow \quad
\left\{ \begin{alignedat}{1}
    & k = m \sin \varphi - n \cos \varphi, \\
    & i = m \cos \varphi + n \sin \varphi. \\
\end{alignedat} \right.
$$
Домножив первое уравнение на~$\sin z \sec \delta$ ($K$), а~второе на~$\cos z \sec \delta$ ($I$), и~сложив, получим:
$$
i I + k K = m \cos(\varphi - z) + n \sec \delta \sin (\varphi - z) = m + n \tg \delta.
$$
Итого запишем формулу Бесселя:
$$
\alpha = T + U + m + n \tg \delta + c \sec \delta.
$$
В~формуле Майера для~каждой звезды было три индивидуальных параметра ($I$, $K$, $C$),
в то~время как в~формуле Бесселя лишь два ($\sec \delta$, $\tg \delta$).
\textit{Ощутимое преимущество при~вычислениях без~ЭВМ!}

\subsection{Определение координат}
\subsubsection{Абсолютное определение прямых восхождений}
\noindent Используются пассажный инструмент, меридианный круг и~часы.
Основная идея метода: $\alpha = S$ в~случае верхней кульминации и~$\alpha = S - 12^h$ в~случае нижней кульминации.
Несмотря на~простоту идеи, практическая реализация отличается сложностью и~трудоемкостью.

\medskip
\noindent\textbf{Этап I. Построение шкалы звездного времени.}\\
По~определению звездное время $S = t_{\gamma}$.
Практическая реализация $S = T + U$, где~$T$~--- показания рабочих часов, $U$~--- поправка часов.
Необходимо определить~$U$.
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*]
    \item Суточный ход часов\footnote{в формуле 24 звездных часа!} $\omega = \dfrac{T_2 - T_1}{24^h}$,
        где $T_2$, $T_1$~--- моменты последовательной одноименной кульминации.
    \item $\dot{U} = \omega = \text{const}$ (астрономические часы должны отвечать этому требованию).\\
        $U(T) = U(T_0) + \omega(T - T_0)$, величина $U(T_0) \equiv U_0$ задает нуль-пункт шкалы~$S$.
        Нуль-пункт выбирают в~точке~$\gamma$.
        По~определению, $\gamma$~--- точка пересечения эклиптики и~экватора,
        то~есть нужно наблюдать Солнце (или~планеты относительно Солнца).
        Измеряем по~крайней мере два~раза абсолютное склонение Солнца.\\
        $\tg \delta_{\odot} = \sin \alpha_{\odot} \tg \varepsilon$, где~$\varepsilon$~--- наклон эклиптики к~экватору.\\
        $\alpha_{\odot} = T + U_0 + \omega \, (T - T_0)$, получаем систему уравнений\\
        $\tg \delta_i = \tg \varepsilon \, \sin(T + U_0 + \omega \, (T - T_0))$, $i=1,2...$
        и~из~ее~решения можно получить $\varepsilon$ и~$U_0$.
        Однако, этот путь весьма сложен, и~поэтому на~практике используется иной способ:
        \begin{enumerate}[noitemsep,topsep=0pt]
            \item Определяют регулярно $\omega$ по 36 звездам ($|\delta| \le 40^{\circ}$, $\alpha \in [0^h, 24^h)$).
            \item $U_0$ считаем произвольным.
            \item Наблюдения Солнца и планет выделяют в отдельную задачу с целью улучшения нуль-пункта каталога.
        \end{enumerate}
\end{enumerate}

\medskip
\noindent\textbf{Этап II. Наблюдения.}\\
Регистрация прохождений через вертикальные нити сетки нитей.
Осреднение всех моментов дает~$T$~--- момент прохождения звезды через "инструментальный меридиан"{}.
Техники регистрации: "глаз-ухо"{}, "глаз-кнопка"{}, контактный микрометр, фотоэлектрический метод, ПЗС.
Последние два лишены влияния человеческого фактора.

\medskip
\noindent\textbf{Этап III. Учет ошибок ориентации инструмента.}\\
Формула Майера $\alpha = T + U + i \, I + k \, K + c \, C$.
Для определения $I$, $K$, $C$ нужно знать $\delta$ и $\varphi$, но большая точность не требуется, достаточно минут.\\
$i$~--- наклонность горизонтальной оси, измеряется уровнем.\\
$c$~--- коллимация, измеряется коллиматорам, установленным в~меридиане инструмента на~высоте его горизонтальной оси.\\
$k$~--- азимут инструмента, измеряется с~помощью наблюдения звезд в~двух кульминациях.\\
Учтем все измерения с~помощью приведенного момента времени $T' = T + \omega (T - T_0) + i \, I + k \, K + c \, C$,
тогда формула Майера примет вид $\alpha = T' + U_0$.

\medskip
\noindent\textbf{Этап IV. Выравнивание прямых восхождений.}\\
Наблюдали $n$~звезд. Тогда имеем уравнения $\alpha_i = T'_i + U_0$ ($i=1,2,...,n$).
То~есть $n$ уравнений при~$n+1$ неизвестном. Решается с~точностью до~константы.\\
Обычно прямые восхождения привязывают к~нуль-пункту уже~существующего каталога:\\
$\alpha_{\text{кат}} + \Delta \alpha + \Delta A = T' + U_0$,
где~$\Delta \alpha$~--- индивидуальная поправка, $\Delta A$~--- общая поправка (нуль-пункт каталога).
$$
\Delta \alpha = \underbrace{(U_0 - \Delta A)}_\textrm{$U_0'$} - \underbrace{(\alpha_{\text{кат}} - T')}_\textrm{$U$},
$$
$U_0'$~--- неизвестная константа, а~$U$~--- поправка часов \textit{относительно каталога}.
$\Delta \alpha = U_0' - U$. Для~двух звезд $i$, $j$: $\Delta \alpha_i - \Delta \alpha_j = U_j - U_i$ ($\forall$ $i$, $j$).
Добавим условие сохранение нуль-пункта каталога:
$$
\sum \Delta \alpha_k = 0,
$$
тогда решение этой системы дает поправки $\Delta \alpha_i$ такие,
что~$\alpha_i = (\alpha_{\text{кат}})_i + \Delta \alpha_i$, которые относят к~нуль-пункту известного каталога.

\medskip
\noindent\textbf{Этап V. Определение абсолютного азимута.}\\
Используется метод Мир.
Мира~--- лампочка, установленная в~меридиане инструмента на~расстоянии $100 \div 200$ метров от~инструмента.
$k = \varkappa + \Delta k$, $\varkappa$~--- азимут линии мир,
$\Delta k$~--- азимут инструмента относительно линии мир (измеряется напрямую, опускание инструмента в~горизонт, с помощью микрометра).
Азимут меняется из-за сезонных подвижек, колебаний температуры и внешнего воздействия.
Линия мир устойчивее, поэтому не требуется ежедневное измерение азимута.
Азимут линии мир измеряют в~двух последовательных кульминациях $\alpha$ UMi с~интервалом в~$20 \div 30$ суток.\\
$\alpha_{\text{в}} + \Delta \alpha_{\text{в}} = T''_{\text{в}} + U_{\text{в}} + k_{\text{в}} (\varkappa + \Delta k_{\text{в}})$,\\
$\alpha_{\text{н}} + \Delta \alpha_{\text{н}} = T''_{\text{н}} + U_{\text{н}} + k_{\text{н}} (\varkappa + \Delta k_{\text{н}})$,\\
$U_{\text{в}} = U_{\text{н}} = U_0$, $\alpha_{\text{в}}$, $\alpha_{\text{н}}$ --- по~эфемеридам,
$\Delta \alpha_{\text{в}} = \Delta \alpha_{\text{н}}$~--- поправки эфемерид (одинаковые, так~как последовательные кульминации в~один и~тот~же день),
имеем 2~уравнения и~2~неизвестные ($\Delta \alpha - U_0$ и $\varkappa$).
Азимут меняется в~пределах $\sim 10''$ с~периодом около года.

\medskip
\noindent\textbf{Этап VI. Определение нуль-пункта каталога (метод Бесселя).}\\
\begin{minipage}{8cm}
\centering
\includegraphics{../section02/images/EclCoord/EclCoord.pdf}
\end{minipage}\hfill
\begin{minipage}{9cm}
Наблюдения Солнца.
Переход из~эклиптической системы координат в~экваториальную:
$$
M_x(\theta) = \left[ \begin{matrix}
    1 & 0 & 0 \\
    0 & \phantom{-} \cos \theta & \sin \theta \\
    0 & -\sin \theta & \cos \theta \\
\end{matrix} \right],
$$
$$
\left[ \begin{matrix}
    \cos \delta \cos \alpha \\ \cos \delta \sin \alpha \\ \sin \delta
\end{matrix} \right] = \left[ \begin{matrix}
    1 & 0 & 0 \\
    0 & \cos \varepsilon & -\sin \varepsilon \\
    0 & \sin \varepsilon & \phantom{-}\cos \varepsilon \\
\end{matrix} \right] \, \left[ \begin{matrix}
    \cancelto{1}{\cos \beta} \cos \lambda \\ \cancelto{1}{\cos \beta} \sin \lambda \\ \cancelto{0}{\sin \beta}
\end{matrix} \right].
$$
\end{minipage}
$$
\left\{ \begin{alignedat}{1}
    & \cos \delta \cos \alpha = \cos \lambda \\
    & \cos \delta \sin \alpha = \cos \varepsilon \sin \lambda \\
    & \sin \delta = \sin \lambda \sin \varepsilon \\
\end{alignedat} \right.
\quad \Rightarrow \quad
\left\{ \begin{alignedat}{1}
    & \tg \delta = \tg \varepsilon \sin \alpha \\
    & \tg \alpha = \tg \lambda \cos \varepsilon \\
    & \cos \lambda = \cos \delta \cos \alpha \\
    & \sin \delta = \sin \lambda \sin \varepsilon \\
\end{alignedat} \right.
$$
$$
\frac{d \delta}{\cos^2 \delta} = \frac{d \varepsilon}{\cos^2 \varepsilon} \sin \alpha + d \alpha \cos \alpha \tg \varepsilon,
$$
где мы полагаем\\
$\delta = \delta_{\text{н}} - \delta_{\text{эф}} = \Delta \delta$,\\
$\alpha = \alpha_{\text{н}} + \Delta A - \alpha_{\text{эф}} = \Delta \alpha + \Delta A$,\\
$\varepsilon = \varepsilon_{\text{н}} - \varepsilon_{\text{эф}} = \Delta \varepsilon$.\\
Условное уравнение:
$$
\Delta \delta \cos^2 \varepsilon = (\Delta \alpha + \Delta A) \cos \alpha \tg \varepsilon \cos^2 \varepsilon \cos^2 \delta + \Delta \varepsilon \sin \alpha \cos^2 \delta,
$$
где $\Delta \alpha$, $\Delta \delta$~--- из~наблюдений, а~$\Delta A$ и~$\Delta \varepsilon$~--- неизвестные (решаем через МНК).
Целиком выполнялось лишь в~XIX~веке.

\subsubsection{Абсолютное определение склонений}
\noindent Используются вертикальный круг, меридианный круг и часы.\\
\begin{minipage}{8cm}
\centering
\includegraphics{../section02/images/DecCircle/DecCircle.pdf}
\end{minipage}\hfill
\begin{minipage}{9cm}
I~--- нижняя кульминация,\\
II~--- верхняя кульминация к~северу от~зенита,\\
III~--- верхняя кульминация к~югу от~зенита.
$$
\begin{alignedat}{2}
    \text{I: } & z_{\text{н}} + \delta = 180^{\circ} - \varphi \quad & \Rightarrow & \quad \delta = 180^{\circ} - \varphi - z_{\text{н}} \\
    \text{II: }  & z_{\text{в}} = \delta - \varphi \quad & \Rightarrow & \quad \delta = z_{\text{в}} + \varphi \\
    \text{III: } & z_{\text{в}} = \varphi - \delta \quad & \Rightarrow & \quad \delta = \varphi - z_{\text{в}} \\
\end{alignedat}
$$
\end{minipage}
Вертикальный круг и~меридианный круг имеет ошибки ориентации.
$\omega_x = i$, $\omega_y$, $\omega_z = k + c \, \csc z$.
Но~только~$\omega_y$ имеют значение, так~как измеряется склонение.
Основные поправки (существуют и~другие):
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*]
    \item Место зенита (ошибка нулевого отсчета)
    \item Неравномерность деления круга (ошибки деления круга, для их~учета отдельно исследуют круг)
    \item Гнутие инструмента (труба прогибается под~действием силы тяжести)
    \item Рефракция
\end{enumerate}
В общем случае $z' = z_{\text{набл}} + \Delta z_1 + \Delta z_2 + \Delta z_3 + ...$, где~$\Delta z_i$~--- поправки.

\noindent Основная проблема при~определении склонений абсолютным методом~--- широта (требуется знать с~точностью, желаемой для~склонения).
\begin{enumerate}[leftmargin=*,label=\textbf{{\arabic*.}}]
    \item\textbf{Предварительное значение широты}\\
        Наблюдение звезд в кульминации.
        \twoitemscolumn
        {
        Кульминации в зонах I и II:
        $$
        \left\{ \begin{alignedat}{2}
            & \delta_{\text{н}} = 180^{\circ} - \varphi - z'_{\text{н}} \\
            & \delta_{\text{в}} = \varphi + z'_{\text{в}} \\
        \end{alignedat} \right.
        $$
        $$
        \delta_{\text{н}} - \delta_{\text{в}} = 180^{\circ} - 2 \, \varphi - z'_{\text{н}} - z'_{\text{в}} = 0
        $$
        \begin{center}
        \fbox{$\varphi = 90^{\circ} - \dfrac{1}{2} (z'_{\text{н}} + z'_{\text{в}})$}
        \end{center}
        }
        {Кульминации в зонах I и III:
        $$
        \left\{ \begin{alignedat}{2}
            & \delta_{\text{н}} = 180^{\circ} - \varphi - z'_{\text{н}} \\
            & \delta_{\text{в}} = \varphi - z'_{\text{в}} \\
        \end{alignedat} \right.
        $$
        $$
        \delta_{\text{н}} - \delta_{\text{в}} = 180^{\circ} - 2 \, \varphi - z'_{\text{н}} + z'_{\text{в}} = 0
        $$
        \begin{center}
        \fbox{$\varphi = 90^{\circ} - \dfrac{1}{2} (z'_{\text{н}} - z'_{\text{в}})$}
        \end{center}
        }
        Определяется без~значения склонений.
        Наблюдая большое количество звезд в~кульминации, получаем набор значений широт, которые усредняем.
        Также корректируем широту, учитывая движения полюса:
        $$
        \varphi' = <\varphi> - [x(t) \, \cos \lambda - y(t) \, \sin \lambda], \text{ $\lambda > 0$ к~западу.}
        $$
    \item\textbf{Выравнивание склонений}\\
        С~помощью измеренных $z'_{\text{н}}$, $z'_{\text{в}}$ и $\varphi'$ получаем $\delta'_{\text{н}}$ и $\delta'_{\text{в}}$.
        Однако, $\delta'_{\text{н}} \neq \delta'_{\text{в}}$!
        Это происходит из-за того, что не~все ошибки учтены.
        Определим: $z_{\text{н}} = z'_{\text{н}} + \Delta z_{\text{н}}$, $z_{\text{в}} = z'_{\text{в}} + \Delta z_{\text{в}}$,  $\varphi = \varphi' + \Delta \varphi$.
        \twoitemscolumn
        {
        Кульминации в~зонах I и~II:
        $$
        \delta_{\text{н}} - \delta_{\text{в}} = \underbrace{180^{\circ} - 2 \, \varphi - z'_{\text{н}} - z'_{\text{в}}}_\textrm{$0$} + 2 \, \Delta \varphi + \Delta z_{\text{н}} + \Delta z_{\text{в}}
        $$
        $$
        \delta_{\text{н}} - \delta_{\text{в}} = 2 \, \Delta \varphi + \Delta z_{\text{н}} + \Delta z_{\text{в}}
        $$
        }
        {
        Кульминации в~зонах I и~III:
        $$
        \delta_{\text{н}} - \delta_{\text{в}} = \underbrace{180^{\circ} - 2 \, \varphi - z'_{\text{н}} + z'_{\text{в}}}_\textrm{$0$} + 2 \, \Delta \varphi + \Delta z_{\text{н}} - \Delta z_{\text{в}}
        $$
        $$
        \delta_{\text{н}} - \delta_{\text{в}} = 2 \, \Delta \varphi + \Delta z_{\text{н}} - \Delta z_{\text{в}}
        $$
        }
        Полагают (Бессель), что~$\Delta z = \Delta k \, \tg z$ (якобы недоучет рефракции), тогда
        $$
        \left\{ \begin{alignedat}{2}
            & \delta_{\text{н}} - \delta_{\text{в}} = 2 \, \Delta \varphi + \Delta k \, (\tg z_{\text{н}} + \tg z_{\text{в}}) \\
            & \delta_{\text{н}} - \delta_{\text{в}} = 2 \, \Delta \varphi + \Delta k \, (\tg z_{\text{н}} - \tg z_{\text{в}})  \\
        \end{alignedat} \right.
        $$
        Наблюдаем в~двух кульминациях некоторое количество звезд, а~дальше всю~систему решаем МНК и~получаем $\Delta \varphi$ и~$\Delta k$.
    \item\textbf{Получение абсолютных склонений}\\
        Получаем $z_{\text{н}}$, $z_{\text{в}}$, $\varphi$, а~из~них по~первым формулам~--- абсолютное $\delta$.
        В~экваториальной зоне \mbox{($\delta < 90^{\circ} - \varphi$)} звезды имеют лишь одну кульминацию.
        $\Delta \varphi$ и~$\Delta k$ в~эту зону экстраполируем, и поэтому в~конечном счете ошибки определения больше.
        Поэтому чем полярнее обсерватория, тем лучше.
\end{enumerate}
\subsubsection{Относительные методы определения прямых восхождений и склонений}
\noindent Хотя относительные методы хуже по~точности и~требуют результаты работ абсолютных методов,
они являются более простыми в~реализации и~более массовыми.
\subsubsection*{Относительный метод определения прямых восхождений}
\noindent Используются пассажный инструмент, меридианный круг и часы.
Редукционные уравнения~--- формула Майера и~формула Бесселя.

\noindent\textbf{Алгоритм относительных измерений:}
\begin{enumerate}[noitemsep,topsep=0pt,leftmargin=*]
    \item $\alpha_j$, $j=1,2,...,I$~--- определяемые звезды.
    \item $A_k$, $k=1,2,...,K$~--- опорные звезды (в~несколько раз меньше, чем~определяемых).
\end{enumerate}
Все звезды упорядочены по~$S$ и~измеряются по~часам $T_j, T_k$.
Коллимация также определяется лабораторным путем $T' = T + c \, \sec \delta$.
$\delta$ достаточно знать с~точностью до~минуты. Тогда\\
$\alpha = T' + (u + m) + n \, \tg \delta$,\\
$A_k = T' + (u + m) + n \, \tg \delta_k$ ($k=1,2,...,K$),\\
из~МНК получим $(u + m)$, $n$. Однако, "система живет"{}, немного меняя параметры. Действуют иным способом:\\
$A_1 = T'_1 + (u + m) + n \, \tg \delta_1$,\\
$A_2 = T'_2 + (u + m) + n \, \tg \delta_2$,\\
$A_1 - A_2 = T'_1 - T'_2 + n \, (\tg \delta_1 - \tg \delta_2)$, при~этом $\delta_1 \neq \delta_2$,
и~с~помощью этого уравнения определяем $n$.
После этого вычисляем $(u + m)$ для~всех звезд: $(u + m)_k = A_k - T'_k - n \tg \delta_k$. 
Полученные значения $(u + m)$ аппроксимируем некоторой гладкой функцией $f(T')$.
Итого определяем прямое восхождение:
$$
\alpha_j = T_j  + n \, \tg \delta_j + f(T'_j), \; j = 1,2,...,J.
$$
Дифференциальный метод в~узких зонах: $\alpha_j - A_k = T'_j - T'_k \; \Rightarrow \; \alpha_j = A_k + (T'_j - T'_k)$,
работает, так~как звезды близки и~между их~прохождениями параметры не~успевают сильно поменяться.

\subsubsection*{Относительный метод определения склонений}
\noindent Используются вертикальный круг, меридианный круг и~часы.

\noindent Склонение $\delta = M - M_0$, где $M$~--- отсчет звезды на~вертикальном круге,
$M_0$~--- отсчет экватора на~вертикальном круге.
Склонения опорных звезд: $\delta_k$ ($k=1,2...,K$).
Посчитаем нулевой отсчет для каждой опорной звезды: $(M_0)_k = M_k - \delta_k$.
Полученные значения аппроксимируем некоторой гладкой функцией $f(T')$.
Итого определяем склонения звезд как
$$
\delta_j = M_j - f(T_j').
$$
\end{document}
